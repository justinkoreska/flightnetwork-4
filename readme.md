# FlightNetwork v4

This is a reference project for implementing architectural ideas. Lessons learned here will be ported back to the `flightnetwork-microsites` project.

## Getting Started

`> yarn install`

`> yarn start`

`> open http://localhost:8080`
