import moment from "moment";
import SearchService from "services/search";

const defaultState = {
  airports: [],
  lookup: {},
};

const types = {
  LOOKUP: "LOOKUP",
};

const actions = {
  lookup: match => (dispatch, getState, services) => {
    services.airportService.lookup(match)
      .then(airports => )
  },
};

const reducer = (state, action) => {
  switch(action.type) {

    case types.LOOKUP:
      return defaultState;

    default:
      return state || defaultState;
  }
};

export { types, actions, reducer };
