import types from "./types";

const defaultState = {
  selected: "en",
};

const setLocale = (state, action) =>
  Object.assign({}, state, {
    selected: action.payload
  });

const setMessages = (state, action) =>
  Object.assign({}, state, {
    messages: action.payload
  });

export default (state, action) => {
  switch(action.type) {
    case types.SET_LOCALE: return setLocale(state, action);
    case types.SET_MESSAGES: return setMessages(state, action);
    default: return state || defaultState;
  }
};
