
const selectedLocale = state => state.locale.selected;

export default {
  selectedLocale,
};
