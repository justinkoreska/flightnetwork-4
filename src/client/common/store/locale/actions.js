import moment from "moment";
import { push } from "react-router-redux";

import types from "./types";

const setLocale = locale => (dispatch, getState, services) => {
  moment.locale(locale);
  services.localeService
    .fetchLocaleData(locale)
    .then(_ => {
      services.localeService
        .fetchTranslations(locale)
        .then(translations => {
          dispatch({
            type: types.SET_MESSAGES,
            payload: translations,
          });
          dispatch({
            type: types.SET_LOCALE,
            payload: locale,
          });
        });
    });
};

const selectLocale = locale => (dispatch, getState, services) => {
  services.cookieService.setCookie("locale", locale);
  const oldPath = getState().router.location.pathname;
  const newPath = oldPath.replace(/^\/\w{2}-\w{2}/, "");
  dispatch(push(`/${locale}${newPath}`));
};

export default {
  setLocale,
  selectLocale,
};
