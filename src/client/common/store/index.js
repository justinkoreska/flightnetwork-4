import { routerReducer as router } from "react-router-redux";

import { reducer as user } from "./user";
import { reducer as search } from "./search";
import { reducer as locale } from "./locale";
import { reducer as currency } from "./currency";
import { reducer as config } from "./config";

export default {
  router,
  user,
  search,
  locale,
  currency,
  config,
};
