import types from "./types";

const defaultState = {
  selected: "CAD",
};

const setCurrency = (state, action) =>
  Object.assign({}, state, {
    selected: action.payload,
  });

export default (state, action) => {
  switch(action.type) {
    case types.SET_CURRENCY: return setCurrency(state, action);
    default: return state || defaultState;
  }
};
