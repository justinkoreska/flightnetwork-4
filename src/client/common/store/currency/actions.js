import moment from "moment";
import { push } from "react-router-redux";

import types from "./types";

const setCurrency = currency => (dispatch, getState, services) => {
  services.cookieService.setCookie("currency", currency);
  dispatch({
    type: types.SET_CURRENCY,
    payload: currency,
  });
};

export default {
  setCurrency,
};
