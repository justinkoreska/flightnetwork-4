
const selectedCurrency = state => state.currency.selected;

export default {
  selectedCurrency,
};
