import types from "./types";

const defaultState = {
  isSearching: false,
  form: {
    tripType: 1,
    cabinClass: 0,
    legs: [{
      date: null,
      from: "",
      to: "",
    }, {
      date: null,
      from: "",
      to: "",
    }],
    adults: 1,
    children: 0,
    infants: 0,
  },
  display: {
    count: 1000,
  },
  filters: {
    selected: {},
    exclusive: {},
  },
  results: [],
  flexdates: [],
};

const updateLeg = (state, { payload: { index, leg } }) => {
  const newState = Object.assign({}, state);
  Object.assign(newState.form.legs[index], leg);
  return newState;
};

const updateType = (state, { payload: type }) =>{
  const newState = Object.assign({}, state);
  newState.form.tripType = type;
  return newState;
};

const searching = (state, { payload: isSearching }) =>
  Object.assign({}, state, {
    isSearching,
    results: isSearching ? [] : state.results,
    flexdates: isSearching ? [] : state.flexdates,
  });

const results = (state, { payload: results }) =>
  Object.assign({}, state, {
    results,
    display: {
      count: 10,
    },
  });

const flexdates = (state, { payload: flexdates }) =>
  Object.assign({}, state, {
    flexdates,
  });

const displayMore = (state, { payload: count }) =>
  Object.assign({}, state, {
    display: {
      count: state.display.count + count,
    },
  });

const setFilter = (state, { payload: { filterName, value, isSelected } }) => {
  const stringValue = JSON.stringify(value);
  const selected = Object.assign({}, state.filters.selected);
  const filters = (selected[filterName] || [])
    .filter(x =>
      x != stringValue
    );
  if (isSelected)
    filters.push(stringValue);
  selected[filterName] = filters;
  return Object.assign({}, state, {
    filters: {
      ...state.filters,
      selected,
    },
  });
};

const setFilterMode = (state, { payload: { filterName, isExclusive } }) =>
  ({
    ...state,
    filters: {
      ...state.filters,
      exclusive: {
        ...state.filters.exclusive,
        [filterName]: isExclusive,
      },
    },
  });

export default (state, action) => {
  switch(action.type) {
    case types.SEARCHING: return searching(state, action);
    case types.RESULTS: return results(state, action);
    case types.FLEXDATES: return flexdates(state, action);
    case types.UPDATE_LEG: return updateLeg(state, action);
    case types.UPDATE_TYPE: return updateType(state, action);
    case types.DISPLAY_MORE: return displayMore(state, action);
    case types.SET_FILTER: return setFilter(state, action);
    case types.SET_FILTER_MODE: return setFilterMode(state, action);
    default: return state || defaultState;
  }
};
