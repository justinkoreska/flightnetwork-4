import moment from "moment";

class Filter {

  constructor(selected, exclusive) {
    this.selected = selected;
    this.exclusive = exclusive;
  }

  reduce(result) {
    return [result];
  }

  match(result) {
    if (!this.selected)
      return false;

    const options = this.reduce(result);
    const strategy = this.exclusive ? "some" : "every";

    return options[strategy](option => {
      const optionString = JSON.stringify(option);
      return this.selected.some(selected =>
        selected == optionString
      );
    });
  }
}

class MultiAirlineFilter extends Filter {

  reduce(result) {
    return [
      result.legs
        .reduce((all, leg) =>
          leg.segments.reduce((all, segment) => {
            if (!all.some(x => x.id == segment.airline.id))
              all.push(segment.airline);
            return all;
          }, all)
        , [])
        .sort((a, b) =>
          a.id > b.id ? 1 : -1
        )
    ];
  }
}

class AirlineFilter extends Filter {

  reduce(result) {
    const airlinesById = result.legs
      .reduce((airlines, leg) => {
        leg.segments
          .forEach(({ airline, airline: { id } }) =>
            airlines[id] = airline
          );
        return airlines;
      }, {});
    return Object
      .values(airlinesById)
      .sort((a, b) =>
        a.name > b.name ? 1 : -1
      );
  }
}

class StopsFilter extends Filter {

  reduce(result) {
    return [
      Math.max(
        ...result.legs.map(leg =>
          leg.segments.length - 1
        )
      )
    ];
  }
}

class DepartureTimesFilter extends Filter {

  constructor(selected, exclusive) {
    super(selected, exclusive);
    this.exclusive = true;
  }

  static period = {
    invalid: -1,
    morning: 0,
    afternoon: 1,
    evening: 2,
    night: 3,
  }

  periodForTime(time) {
    const t = moment(time);
    const period = DepartureTimesFilter.period;

    if (t.isValid()) {
      const minutes = t.hour() * 60 + t.minute();

      if (minutes > 1440) return period.invalid;
      if (minutes > 1080) return period.evening;
      if (minutes > 720) return period.afternoon;
      if (minutes > 300) return period.morning;
      if (minutes > 0) return period.night;
    }

    return period.invalid;
  }

  reduce(result) {
    return result.legs
      .reduce((times, leg, legIndex) => {
        times.push({
          leg: legIndex,
          period: this.periodForTime(leg.segments[0].departTime),
        });
        return times;
      }, []);
  }
}

export default {
  airline: AirlineFilter,
  stops: StopsFilter,
  departure: DepartureTimesFilter,
};
