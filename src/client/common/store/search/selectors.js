import moment from "moment";
import { createSelector } from "reselect";

import filterTypes from "./filters";

const results = state => state.search.results;
const flexdates = state => state.search.flexdates;

const selectedFilters = state => state.search.filters.selected;
const exclusiveFilters = state => state.search.filters.exclusive;

const displayCount = state => state.search.display.count;

const fromDate = state => state.search.form.legs[0].date;
const toDate = state => state.search.form.legs[state.search.form.legs.length-1].date;
const fromAirport = state => state.search.form.legs[0].from;
const toAirport = state => state.search.form.legs[0].to;
const tripType = state => state.search.form.tripType;
const isSearching = state => state.search.isSearching;

const isValid = createSelector(
  fromDate,
  toDate,
  fromAirport,
  toAirport,
  isSearching,
  (fromDate, toDate, fromAirport, toAirport, isSearching) =>
    fromDate && toDate && fromAirport && toAirport && !isSearching
);

const mapResult = result => ({
  id: result.SequenceNumber,
  isBest: result.AirItineraryPricingInfo.TPA_Extensions.BestItinerary,
  priority: result.AirItineraryPricingInfo.TPA_Extensions.PriorityScore,
  price: result.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount,
  currency: result.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode,
  legs: result.AirItinerary.OriginDestinationOptions.OriginDestinationOption.map(leg => ({
    segments: leg.FlightSegment.map(segment => ({
      airline: {
        id: segment.MarketingAirline.Code,
        name: segment.MarketingAirline.CompanyShortName,
        image: `https://img.flightnetwork.com/b2c-v2/images-hi/${segment.MarketingAirline.Code}.png`,
      },
      flightNumber: segment.FlightNumber,
      departAirport: segment.DepartureAirport.LocationCode,
      departTime: moment(segment.DepartureDateTime),
      arriveAirport: segment.ArrivalAirport.LocationCode,
      arriveTime: moment(segment.ArrivalDateTime),
      duration: parseDuration(segment.JourneyDuration),
    })),
  })),
});

const parseDuration = str => {
  const durationRegex = /^PT((\d+)H)?(\d+)M$/;

  if ("string" != typeof str || !durationRegex.test(str))
    return 0;

  let duration = 0;

  const [
    durationMatch,
    hourPart,
    hours = "0",
    minutes = "0",
  ] = str.match(durationRegex);

  duration += parseInt(hours, 10) * 60;
  duration += parseInt(minutes, 10);

  return duration;
};

const mapDurations = itinerary => ({
  ...itinerary,
  legs: itinerary.legs.map(leg => ({
    ...leg,
    totalDuration: leg.segments.reduce((duration, segment, index) => {
        let segmentDuration = segment.duration;
        if (index < leg.segments.length-1) {
          const nextSegment = leg.segments[index+1];
          const connectionTime = nextSegment.departTime.diff(segment.arriveTime, "seconds");
          segmentDuration += connectionTime;
        }
        return duration + segmentDuration;
      }, 0),
    flightDuration: leg.segments.reduce((duration, segment) =>
        duration + segment.duration
      , 0),
  })),
});

const mappedResults = createSelector(
  results,
  results => results
    .map(mapResult)
    .map(mapDurations)
);

const resultsView = createSelector(
  mappedResults,
  displayCount,
  (results, count) =>
    results
      .slice(0, count)
);

const flexdatesView = createSelector(
  flexdates,
  flexdates =>
    flexdates
      .map(flexdate => ({
        ...flexdate,
        departDate: moment(flexdate.departs, "YYYYMMDD"),
        returnDate: moment(flexdate.returns, "YYYYMMDD"),
      }))
);

const filtersView = createSelector(
  mappedResults,
  results => {
    const filters = results
      .reduce((filters, result) => {
        Object
          .keys(filterTypes)
          .forEach(filterName => {
            filters[filterName] = filters[filterName] || {};
            const filter = new filterTypes[filterName]();
            const options = filter.reduce(result) || [];
            options.forEach(option => {
              const optionString = JSON.stringify(option);
              filters[filterName][optionString] = option;
            });
          });
        return filters;
      }, {});
    Object
      .keys(filters)
      .forEach(filterName =>
        filters[filterName] = Object.values(filters[filterName])
      );
    return filters;
  }
);

const filteredResults = createSelector(
  mappedResults,
  selectedFilters,
  exclusiveFilters,
  (results, selected, exclusive) =>
    results.filter(result =>
      Object
        .keys(selected)
        .reduce((match, filterName) => {
          let filter;
          try {
            filter = new filterTypes[filterName](selected[filterName], exclusive[filterName]);
          } catch(e) { console.log(e); }
          return match && filter && !filter.match(result);
        }, true)
    )
);

const groupResults = results => {
  const grouped = {};

  results.forEach(result => {
    const keys = [];
    result.legs.forEach(leg =>
      leg.segments.forEach(segment => {
        const {
          departAirport,
          arriveAirport,
          airline: { code: airlineCode },
        } = segment;
        keys.push(
          `${airlineCode}-${departAirport}-${arriveAirport}`
        );
      })
    );
    const key = keys.join("-");
    grouped[key] = grouped[key] || [];
    grouped[key].push(result);
  });

  return Object.values(grouped);
};

const resultsGroupedView = createSelector(
  filteredResults,
  displayCount,
  (results, count) =>
    groupResults(results)
      .slice(0, count)
      .map(group => ({
        ...group[0],
        group,
      }))
);

const tripDuration = itinerary =>
  itinerary.legs.reduce((duration, leg) =>
    duration + leg.totalDuration
  , 0);

const sortByPriceDurationPriority = results => {
  const max = {
    price: Math.max(...results.map(r => r.price)) || 1,
    duration: Math.max(...results.map(tripDuration)) || 1,
    //priority: Math.max(...results.map(r => r.priority)) || 1,
  };
  const factor = fare =>
    ["price", "duration"]//, "priority"]
      .reduce((factor, facet) =>
        factor + (fare[facet] / max[facet])
      , 0);
  return results
    .sort((a, b) => a.priority - b.priority)
    .sort((a, b) => factor(b) - factor(a));
};

const groupByDepartAirline = results => {
  const grouped = {};

  results.forEach(result => {
    const { airline: { code: airlineCode } } = result.legs[0].segments[0];
    grouped[airlineCode] = grouped[airlineCode] || [];
    grouped[airlineCode].push(result);
  });

  return grouped;
};

const groupByDepartFlights = results => {
  const grouped = {};

  results.forEach(result => {
    const key = result.legs[0]
      .segments
      .map(segment => segment.flightNumber)
      .join("-");
    grouped[key] = grouped[key] || [];
    grouped[key].push(result);
  });

  return Object
    .keys(grouped)
    .map(key => ({
      ...grouped[key][0],
      itineraries: grouped[key],
    }));
};

const hasRequestedAirports = (itinerary, departAirport, arriveAirport) => {
  const firstLeg = itinerary.legs[0];
  const lastLeg = itinerary.legs[itinerary.legs.length-1];
  const firstSegment = leg => leg.segments[0];
  const lastSegment = leg => leg.segments[leg.segments.length-1];

  return firstSegment(firstLeg).departAirport == departAirport
    && lastSegment(firstLeg).arriveAirport == arriveAirport
    && firstSegment(lastLeg).departAirport == arriveAirport
    && lastSegment(lastLeg).arriveAirport == departAirport;
};

const requestedAirportsResults = createSelector(
  filteredResults,
  fromAirport,
  toAirport,
  (results, from, to) =>
    results.filter(i => hasRequestedAirports(i, from.code, to.code))
);

const sortedResults = createSelector(
  requestedAirportsResults,
  sortByPriceDurationPriority
);

const resultsByAirlineView = createSelector(
  sortedResults,
  displayCount,
  (results, count) => {
    const grouped = groupByDepartAirline(results);
    return Object
      .keys(grouped)
      .map(airline => ({
        airline,
        ...grouped[airline][0],
        itineraries: groupByDepartFlights(grouped[airline]),
      }))
      .sort((a, b) => a.price - b.price);
  }
);

const alternateResults = createSelector(
  filteredResults,
  fromAirport,
  toAirport,
  (results, from, to) =>
    results.filter(i => !hasRequestedAirports(i, from.code, to.code))
);

const bestResult = createSelector(
  filteredResults,
  results =>
    results.find(f => f.isBest)
);

const fastestResult = createSelector(
  requestedAirportsResults,
  results =>
    results
      .sort((a, b) =>
        tripDuration(a) - tripDuration(b)
      )
      .find(x => true)
);

const cheapestResult = createSelector(
  requestedAirportsResults,
  results =>
    results
      .sort((a, b) =>
        a.price - b.price
      )
      .find(x => true)
);

export default {
  displayCount,
  resultsView,
  resultsGroupedView,
  fromDate,
  toDate,
  fromAirport,
  toAirport,
  tripType,
  isSearching,
  isValid,
  filtersView,
  selectedFilters,
  exclusiveFilters,
  resultsByAirlineView,
  bestResult,
  fastestResult,
  cheapestResult,
  alternateResults,
  flexdatesView,
};
