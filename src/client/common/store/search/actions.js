import { push } from "react-router-redux";
import types from "./types";

const updateLeg = (index, leg) => ({
  type: types.UPDATE_LEG,
  payload: {
    index,
    leg,
  },
});

const updateType = type => ({
  type: types.UPDATE_TYPE,
  payload: type,
});

const startSearching = () => ({
  type: types.SEARCHING,
  payload: true,
});

const stopSearching = () => ({
  type: types.SEARCHING,
  payload: false,
});

const updateResults = results => ({
  type: types.RESULTS,
  payload: results,
});

const updateFlexdates = flexdates => ({
  type: types.FLEXDATES,
  payload: flexdates,
});

const displayMore = count => ({
  type: types.DISPLAY_MORE,
  payload: count,
});

const mapFormToRequest = form => ({
  flex: 1,
  currency: "CAD",
  trip_type: ["Oneway", "Roundtrip", "Multicity"][form.tripType],
  trip_class: ["Economy", "Business"][form.cabinClass],
  Adult: form.adults,
  Child: form.children,
  Infant: form.infants,
  dep_from: form.legs[0].from.code,
  dep_to: form.legs[0].to.code,
  departure_date: form.legs[0].date.format("YYYY-MM-DD"),
  ret_from: form.legs[0].to.code,
  ret_to: form.legs[0].from.code,
  return_date: form.legs[1].date.format("YYYY-MM-DD"),
});

const mapFormToUrl = form =>
  "/search/" + [
    form.legs[0].from.code,
    form.legs[0].to.code,
    form.legs[0].date.format("YYYYMMDD"),
    form.legs[1].date.format("YYYYMMDD"),
  ].join("/");

const search = () => (dispatch, getState, services) => {
  dispatch(startSearching());

  const form = getState().search.form;
  const request = mapFormToRequest(form);
  const url = mapFormToUrl(form);

  dispatch(push(url));

  return services.searchService
    .search(request, results => {
      dispatch(updateResults(results));
    })
    .then(searchId => {
      if (searchId && request.flex)
        return services.searchService
          .getFlexdates(searchId)
          .then(flexdates => {
            dispatch(updateFlexdates(flexdates));
            dispatch(stopSearching());
          });
    })
    .finally(() => {
      dispatch(stopSearching());
    });
};

const setFilter = (filterName, value, isSelected) => ({
  type: types.SET_FILTER,
  payload: {
    filterName,
    value,
    isSelected,
  },
});

const setFilterMode = (filterName, isExclusive) => ({
  type: types.SET_FILTER_MODE,
  payload: {
    filterName,
    isExclusive,
  },
});

export default {
  updateLeg,
  updateType,
  search,
  updateResults,
  startSearching,
  stopSearching,
  displayMore,
  setFilter,
  setFilterMode,
};
