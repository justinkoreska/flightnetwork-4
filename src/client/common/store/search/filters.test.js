const should = require("chai").should();

import moment from "moment";

import filterTypes from "./filters";

const results = [{
  sequence: 999,
  price: 250.00,
  currency: "CAD",
  legs: [{
    segments: [{
      airline: {
        id: "AC",
        name: "Air Canada",
        image: "//img.flightnetwork.com/b2c-user/images/AC.gif",
      },
      flightNumber: "AC1234",
      departAirport: "YYZ",
      departTime: moment("2018-01-01T16:20:00Z"),
      arriveAirport: "SFO",
      arriveTime: moment("2018-01-01T20:40:00Z"),
    }],
  }, {
    segments: [{
      airline: {
        id: "AC",
        name: "Air Canada",
        image: "//img.flightnetwork.com/b2c-user/images/AC.gif",
      },
      flightNumber: "AC4321",
      departAirport: "SFO",
      departTime: moment("2018-01-10T16:20:00Z"),
      arriveAirport: "YYZ",
      arriveTime: moment("2018-01-10T20:40:00Z"),
    }],
  }],
}];

describe("Filters module", () => {

  describe("Airlines filter", () => {

    it("should show AC", () => {

      const filter = {
        name: "airline",
        selected: [
          "WS",
        ],
      };

      const filterObject = new filterTypes[filter.name](filter.selected);
      const filtered = results.filter(result => filterObject.match(result));

      filtered.length.should.equal(1);
    });
  });

});

/*

given:
1 AB BC CD
2 AB CD
3 AB
4 CD
0=inclusive
1=exclusive

test:
0[]
1, 2, 3, 4

0[CD]
1, 2, 3

1[CD]
3

0[AB, CD]
1

1[AB, CD]
-

*/