import UserService from "services/user";

const defaultState = {
  isAuthenticated: false,
  isLoading: false,
  data: null,
};

const types = {
  SIGNIN: "SIGNIN",
  SIGNIN_LOADING: "SIGNIN_LOADING",
  SIGNIN_ERROR: "SIGNIN_ERROR",
  SIGNOUT: "SIGNOUT",
};

const actions = {
  signin: (username, password) => (dispatch, getState, services) => {
    dispatch({ type: types.SIGNIN_LOADING });
    services.userService.signin(username, password)
      .then(user => {
        dispatch({
          type: types.SIGNIN,
          payload: { username, token: user.token },
        });
      })
      .catch(error => {
        dispatch({
          type: types.SIGNIN_ERROR,
          payload: error,
        });
      });
  },
  signout: () => ({
    type: types.SIGNOUT,
  }),
};

const reducer = (state, action) => {
  switch(action.type) {

    case types.SIGNIN:
      return Object.assign({}, state, {
        isAuthenticated: true,
        isLoading: false,
        data: action.payload,
      });

    case types.SIGNIN_LOADING:
      return Object.assign({}, defaultState, {
        isLoading: true,
      });

    case types.SIGNIN_ERROR:
      return Object.assign({}, defaultState, {
        error: action.payload,
      });

    case types.SIGNOUT:
      return defaultState;

    default:
      return state || defaultState;
  }
};

export { types, actions, reducer };
