import { toQueryString } from "modules/url";
import { jsonHandler } from "modules/fetch-handler";

class AirportService {

  constructor(config, fetch) {
    this.fnxmlBase = config.fnxmlBase;
    this.fetch = fetch;
    this._airports = {};
  }

  find(filter) {
    if (this._airports[filter])
      return new Promise(resolve =>
        resolve(this._airports[filter])
      );

    const query = toQueryString({
      query_string: filter,
      location_type: "origin",
      max_results: 10,
      lang: "EN",
    });
    const url = this.fnxmlBase + "/location/search/json?" + query;

    return this
      .fetch(url)
      .then(jsonHandler)
      .then(this.mapAirports)
      .then(airports => this._airports[filter] = airports);
  }

  mapAirports(json) {
    if (json.Errors)
      throw new Error(json.Errors.Message);

    const {
      LocationRS: {
        Locations: locations = []
      } = {}
    } = json;

    return locations.map(airport => ({
      code: airport.AirportCode,
      name: airport.AirportName,
      city: airport.CityName,
      region: airport.ProvinceName,
      country: airport.CountryName,
    }));
  }
}

export default AirportService;
