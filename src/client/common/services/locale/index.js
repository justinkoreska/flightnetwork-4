import { addLocaleData } from "react-intl";
import { parseLocale } from "modules/locale";

import ReactIntlLocaleData from "./react-intl";

class LocaleService {

  constructor(config, fetch) {
    this.config = config;
    this.fetch = fetch;
    this._translations = {};
  }

  fetchTranslations(locale) {
    const { language, region } = parseLocale(locale);

    return new Promise((resolve, reject) => {

      const makeFetch = key =>
        this.fetch(`/assets/${key}.json`)
          .then(response => response.ok ? response.json() : {})
          .then(json => this._translations[key] = json)
          .catch(e => ({}));

      const fetchLanguage = this._translations[language]
        ? new Promise(resolve => resolve(this._translations[language]))
        : makeFetch(language);
      const fetchLocale = this._translations[locale]
        ? new Promise(resolve => resolve(this._translations[locale]))
        : makeFetch(locale);

      const merge = fetches =>
        Object.assign({}, fetches[0], fetches[1]);

      Promise
        .all([fetchLanguage, fetchLocale])
        .then(fetches => resolve(merge(fetches)));
    });
  }

  fetchLocaleData(locale) {
    const { language, region } = parseLocale(locale);
    //TODO check language exists
    return ReactIntlLocaleData[language]()
      .then(intlData =>
        addLocaleData(intlData)
      );
  }
}

export default LocaleService;
