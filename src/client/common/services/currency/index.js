import { jsonHandler } from "modules/fetch-handler";

class CurrencyService {

  constructor(config, fetch) {
    this.config = config;
    this.fetch = fetch;
    this._rates = {};
    this.fetchRates();
  }

  getConverter(fromCurrency, toCurrency) {
    const rates = this._rates;

    return value => {
      let rate = rates[toCurrency] || 1;

      if (fromCurrency !== rates.base) {
        const baseRate = rates[fromCurrency] || 1;
        rate /= baseRate;
      }

      return value * rate;
    };
  }

  fetchRates() {
    return new Promise((resolve, reject) => {
      if (this._rates.base)
        return resolve(this._rates);

      this
        .fetch(this.config.fnxmlBase + "/exchangerate/from/USD")
        .then(jsonHandler)
        .then(exchange => resolve(this._rates = this.mapExchange(exchange)));
    });
  }

  fetchTop() {
  }

  mapExchange(exchange) {
    return {
      base: exchange.basecode,
      ...exchange.rates.reduce((rates, rate) => {
        rates[rate.code] = rate.factor;
        return rates;
      }, {}),
    };
  }
}

export default CurrencyService;
