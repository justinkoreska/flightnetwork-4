import { toQueryString } from "modules/url";
import { jsonHandler } from "modules/fetch-handler";

const DEFAULT_POLL_SECONDS = 3;
const DEFAULT_TIMEOUT_SECONDS = 60;

class OtaSearchService {

  constructor(config, fetch) {
    const {
      fnxmlBase,
      clientRef,
      search: {
        pollSeconds,
        timeoutSeconds,
      } = {},
    } = config;

    this._apiBase = fnxmlBase || "";
    this._clientRef = clientRef || "flightnetwork";
    this._pollSeconds = pollSeconds || DEFAULT_POLL_SECONDS;
    this._timeoutSeconds = timeoutSeconds || DEFAULT_TIMEOUT_SECONDS;

    this.fetch = fetch;
  }

  search(request, callback) {

    request.client_ref = this._clientRef;

    this.isSearching = true;

    const url = this._apiBase + "/search/async/json";
    const body = toQueryString(request);

    return this
      .fetch(url, {
        method: "POST",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        body,
      })
      .then(jsonHandler)
      .then(json => {
        this.isSearching = false;

        const results = json.OTA_AirLowFareSearchRS;

        if (!results)
          throw new Error("Unexpected response");
        if (results.Errors)
          throw new Error(results.Errors.Message);

        this._started = new Date();
        this._searchId = results.EchoToken;
        this._status = results.Status;

        return this._fetchResults(this._searchId, callback);
      })
      .catch(error => {
        this.isSearching = false;
        this.error = error;
        throw error;
      });
  }

  _fetchResults(sid, callback) {

    const url = this._apiBase + "/search/results/json";
    const body = toQueryString({ sid, limit: 0 });

    return this
      .fetch(url, {
        method: "POST",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        body,
      })
      .then(jsonHandler)
      .then(json => {
        const results = json.OTA_AirLowFareSearchRS;

        if (!results)
          throw new Error("Unexpected response");
        if (results.Errors)
          throw new Error(results.Errors.Message);

        this._status = results.Status;
        this._itineraries = results.PricedItineraries;

        const isTimeout = new Date() - this._started > this._timeoutSeconds * 1000;
        const isComplete = "Completed" == this._status;
        const done = isComplete || isTimeout;

        if ("function" == typeof callback)
          callback(this._itineraries);

        if (done)
          return sid;

        return new Promise(resolve => {
          setTimeout(() => {
            resolve(this._fetchResults(sid, callback));
          }, this._pollSeconds * 1000);
        });
      });
  }

  getFlexdates(sid) {

    const url = this._apiBase + "/flexdata/" + sid;

    return this
      .fetch(url)
      .then(jsonHandler)
      .then(mapFlexdates)
      .then(flexdates => {

        if (!flexdates)
          throw new Error("Unexpected response");
        if (flexdates.Errors)
          throw new Error(flexdates.Errors.Message);

        return this._flexdates = flexdates;
      });
  }

}

const mapFlexdates = flexdata =>
  flexdata
    .reduce((mapped, fare) => {
      ["connection", "direct"].forEach(type => {
        mapped.push({
          departs: fare.depart_date,
          returns: fare.return_date,
          airline: fare[type].carrier,
          price: fare[type].fare.source_fare,
          currency: fare[type].fare.source_currency,
          isNonstop: "direct" == type,
        });
      });
      return mapped;
    }, [])
    .sort((a, b) =>
      a.price - b.price
    );

export default OtaSearchService;
