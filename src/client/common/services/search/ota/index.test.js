import { expect } from "chai";
import sinon from "sinon";

import OtaSearchService from ".";

describe("OtaSearchService", () => {

  it("should return a promise that resolves only when search is complete", () => {

    const config = {
      search: {
        pollSeconds: 0.001,
      },
    };
    const request = {};

    let resultsCallCount = 0;

    const fetchFake = sinon.fake(
      url => new Promise(resolve =>
        resolve({
          ok: true,
          json: () => ({
            OTA_AirLowFareSearchRS: {
              Status: /results/.test(url) && ++resultsCallCount >= 3
                ? "Completed"
                : "Pending",
            },
          }),
        })
      )
    );

    const searchService = new OtaSearchService(config, fetchFake);

    return searchService
      .search(request)
      .then(() => {
        expect(searchService.error).to.be.undefined;
        expect(searchService.isSearching).to.be.false;
        expect(fetchFake.callCount).to.equal(4);
      });
  });

  it("should fetch the correct roundtrip URL", () => {
    const config = {
      fnxmlBase: "http://test.com/api",
    };
    const request = {
      trip_type: "Roundtrip",
      trip_class: "Economy",
      Adult: 3,
      Child: 2,
      Infant: 1,
      dep_from: "ABC",
      dep_to: "XYZ",
      departure_date: "2006-08-26",
      ret_from: "XYZ",
      ret_to: "ABC",
      return_date: "2008-08-26",
      client_ref: "flightnetwork",
      currency: "CAD",
    };

    const fetchSpy = sinon.spy(
      () => new Promise(resolve =>
        resolve({
          ok: true,
          json: () => ({}),
        })
      )
    );

    const searchService = new OtaSearchService(config, fetchSpy);
    searchService.search(request).catch(e => {});

    expect(fetchSpy.args).to.have.lengthOf(1);
    expect(fetchSpy.args[0]).to.have.lengthOf(2);

    expect(fetchSpy.args[0][0]).to.be.a("string");
    expect(fetchSpy.args[0][1]).to.be.a("object");

    expect(fetchSpy.args[0][1]).to.have.property("body");
    expect(fetchSpy.args[0][1]).to.have.property("method");
    expect(fetchSpy.args[0][1]).to.have.nested.property("headers.content-type");

    expect(fetchSpy.args[0][0]).to.equal(
      "http://test.com/api/search/async/json"
    );
    expect(fetchSpy.args[0][1].headers["content-type"]).to.equal(
      "application/x-www-form-urlencoded"
    );
    expect(fetchSpy.args[0][1].method).to.equal(
      "POST"
    );
    expect(fetchSpy.args[0][1].body).to.equal(
      "trip_type=Roundtrip&trip_class=Economy&Adult=3&Child=2&Infant=1&dep_from=ABC&dep_to=XYZ&departure_date=2006-08-26&ret_from=XYZ&ret_to=ABC&return_date=2008-08-26&client_ref=flightnetwork&currency=CAD"
    );
  });

});
