import { toQueryString } from "modules/url";
import { jsonHandler } from "modules/fetch-handler";

const DEFAULT_POLL_SECONDS = 1;
const DEFAULT_TIMEOUT_SECONDS = 60;

class EfoeSearchService {

  constructor(config, fetch) {
    const {
      fnxmlBase,
      search: {
        pollSeconds,
        timeoutSeconds,
      } = {},
    } = config;

    this._apiBase = fnxmlBase || "";
    this._pollSeconds = pollSeconds || DEFAULT_POLL_SECONDS;
    this._timeoutSeconds = timeoutSeconds || DEFAULT_TIMEOUT_SECONDS;

    this.fetch = fetch;
  }

  search(request, callback) {

    this.isSearching = true;

    const url = this._apiBase + "/search/async/json";
    const body = toQueryString(request);

    return this
      .fetch(url, {
        method: "POST",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        body,
      })
      .then(jsonHandler)
      .then(json => {
        this.isSearching = false;

        const results = json.OTA_AirLowFareSearchRS;

        if (!results)
          throw new Error("Unexpected response");
        if (results.Errors)
          throw new Error(results.Errors.Message);

        this._started = new Date();
        this._searchId = results.EchoToken;
        this._status = results.Status;

        return this._fetchResults(this._searchId, callback);
      })
      .catch(error => {
        this.isSearching = false;
        this.error = error;
        throw error;
      });
  }

  _fetchResults(sid, callback) {

    const url = this._apiBase + "/search/results/json";
    const body = toQueryString({ sid, limit: 0 });

    return this
      .fetch(url, {
        method: "POST",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        body,
      })
      .then(jsonHandler)
      .then(json => {
        const results = json.OTA_AirLowFareSearchRS;

        if (!results)
          throw new Error("Unexpected response");
        if (results.Errors)
          throw new Error(results.Errors.Message);

        this._status = results.Status;
        this._itineraries = results.PricedItineraries;

        const isTimeout = new Date() - this._started > this._timeoutSeconds * 1000;
        const isComplete = "Completed" == this._status;
        const done = isComplete || isTimeout;

        if ("function" == typeof callback)
          callback(this._itineraries);

        if (done)
          return sid;

        return new Promise(resolve => {
          setTimeout(() => {
            resolve(this._fetchResults(sid, callback));
          }, this._pollSeconds * 1000);
        });
      });
  }

}

export default EfoeSearchService;
