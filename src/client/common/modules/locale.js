
const parseLocale = locale => {
  if (2 == locale.length) {
    return {
      language: locale
    };
  } else {
    const parts = locale.split(/[_-]/);
    if (2 == parts.length) {
      return {
        language: parts[0],
        region: parts[1],
      };
    }
  }
  return {};
};

export {
  parseLocale,
};
