import React from "react";
import { connect } from "react-redux";
import { FormattedNumber } from "react-intl";

import inject from "modules/inject";
import { selectors } from "store/currency";

@inject
class Price extends React.Component {

  render() {
    const {
      price,
      currency,
      //format = "floatCurrency",
      selectedCurrency,
    } = this.props;
    const { currencyService } = this.context.services;

    const converter = currencyService.getConverter(currency, selectedCurrency);
    const convertedPrice = converter(price);

    return (
      <FormattedNumber
        value={convertedPrice}
        currency={selectedCurrency}
        //format={format}
        style="currency"
      />
    );
  }
}

// Price.propTypes = {
//   currency: PropTypes.string.isRequired,
//   format: PropTypes.string,
// };

const mapStateToProps = state => ({
  selectedCurrency: selectors.selectedCurrency(state),
});

export default connect(mapStateToProps)(Price);
