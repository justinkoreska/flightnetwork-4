import React from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";

import { actions, selectors } from "store/search";

import AirportInput from "components/airport-input";
import PassengersInput from "components/passengers-input";
import DateInput from "components/date-input";
import Spinner from "components/spinner";

import "./scss/index.scss";
import locale from "./messages";

const SearchFormView = ({
  intl,
  updateFromAirport,
  updateToAirport,
  updateFromDate,
  updateToDate,
  updateType,
  updatePassengers,
  fromAirport,
  toAirport,
  fromDate,
  toDate,
  tripType,
  search,
  isSearching,
  isValid,
}) => (
  <div className="SearchForm">
    <form>
      <div className="flex one two-600 four-1000">
        <AirportInput
          placeholder={intl.formatMessage(locale.AirportFrom)}
          onSelect={updateFromAirport}
          value={fromAirport}
        />
        <AirportInput
          placeholder={intl.formatMessage(locale.AirportTo)}
          onSelect={updateToAirport}
          value={toAirport}
        />
        <DateInput
          placeholder={intl.formatMessage(locale.DateDepart)}
          onSelect={updateFromDate}
          value={fromDate}
          anchor={toDate}
        />
        <DateInput
          placeholder={intl.formatMessage(locale.DateReturn)}
          onSelect={updateToDate}
          value={toDate}
          anchor={fromDate}
        />
      </div>
      <div className="flex one two-600 four-1000">
        <div>
          <label>
            <input
              type="checkbox"
              checked={tripType}
              onChange={e => updateType(e.target.checked ? 1 : 0)}
            />
            <span className="checkable">Round-trip</span>
          </label>
        </div>
      </div>
      <div>
        <button
          className="SearchForm-search"
          onClick={search}
          disabled={!isValid}
        >
          Search
        </button>
        <Spinner spinning={isSearching} />
      </div>
    </form>
  </div>
);

const mapStateToProps = state => ({
  fromDate: selectors.fromDate(state),
  toDate: selectors.toDate(state),
  fromAirport: selectors.fromAirport(state),
  toAirport: selectors.toAirport(state),
  tripType: selectors.tripType(state),
  isSearching: selectors.isSearching(state),
  isValid: selectors.isValid(state),
});

const mapDispatchToProps = dispatch => ({
  updateFromAirport: from => dispatch(actions.updateLeg(0, { from })),
  updateToAirport: to => dispatch(actions.updateLeg(0, { to })),
  updateFromDate: date => dispatch(actions.updateLeg(0, { date })),
  updateToDate: date => dispatch(actions.updateLeg(1, { date })),
  updateType: type => dispatch(actions.updateType(type)),
  updatePassengers: console.log,
  search: e => {
    e.preventDefault();
    dispatch(actions.search());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(SearchFormView));
