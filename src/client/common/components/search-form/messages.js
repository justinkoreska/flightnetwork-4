
export default {
  AirportFrom: {
    id: "From",
  },
  AirportTo: {
    id: "To",
  },
  DateDepart: {
    id: "Depart",
  },
  DateReturn: {
    id: "Return",
  },
};
