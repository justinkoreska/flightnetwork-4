import React from "react";

class HoverInput extends React.Component {

  constructor(props) {
    super(props);

    for (const func of ["onMouseEnter", "onMouseOut", "onClick"])
      this[func] = this[func].bind(this);

    this.state = {
      value: this.props.canDeselect ? null : props.values[0],
      hover: null,
    };
  }

  className(value) {
    const isActive =
      null === this.state.hover &&
      value <= this.state.value ||
      value <= this.state.hover

    return isActive ? "active" : "";
  }

  onMouseEnter(event, value) {
    this.setState({
      hover: value,
    });
  }

  onMouseOut(event) {
    this.setState({
      hover: null,
    });
  }

  onClick(event, value) {
    if (this.props.canDeselect && value === this.state.value)
      value = null;
    this.setState({
      value,
    });
    this.props.onSelect(value);
  }

  limitMaxValue(values) {
    const hasValue =
      null === this.state.value ||
      values.find(value => value === this.state.value);
    if (!hasValue) {
      const value = values[values.length-1];
      this.setState({
        value,
      });
      this.props.onSelect(value);
    }
  }

  componentWillReceiveProps(props) {
    this.limitMaxValue(props.values);
  }

  render() {
    return (
      <ul className={this.props.className}>
        { this.props.values.map(value =>
           <li
             key={value}
             className={this.className(value)}
             onMouseEnter={event => this.onMouseEnter(event, value)}
             onMouseOut={event => this.onMouseOut(event)}
             onClick={event => this.onClick(event, value)}
             >
             { this.props.children }
           </li>
        )}
      </ul>
    );
  }
}

export default HoverInput;
