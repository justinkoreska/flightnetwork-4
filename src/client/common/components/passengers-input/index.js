import React from "react";

import "./scss/index.scss";
import HoverInput from "./components/hover-input";

class PassengersInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      adultValues: [1,2,3,4,5],
      childValues: [1],
      adult: 1,
      child: null,
    };
  }

  makeSeries(max) {
    return Array.from(Array(max), (e, n) => n + 1);
  }

  onSelectAdult(value) {
    this.setState({
      adult: value,
      childValues: this.makeSeries(value),
    }, this.onChange);
  }

  onSelectChild(value) {
    this.setState({
      child: value
    }, this.onChange);
  }

  onChange() {
    this.props.onChange({
      adult: this.state.adult,
      child: this.state.child,
    });
  }

  render() {
    return (
      <div className="PassengersInput">
        <HoverInput
          className="PassengersInput-adult"
          values={this.state.adultValues}
          onSelect={this.onSelectAdult.bind(this)}
          />
        <HoverInput
          className="PassengersInput-child"
          values={this.state.childValues}
          onSelect={this.onSelectChild.bind(this)}
          canDeselect={true}
          />
      </div>
    );
  }
}

export default PassengersInput;
