import React from "react";

import Price from "components/price";

const airlineLabel = airline =>
  <span key={airline.id}>
    <i className="SearchResult-airlineLogo">
      <img src={airline.image} />
    </i>
    {airline.name}
  </span>

const airlinesLabel = legs =>
  <span>
    {legs
      .reduce((airlines, leg) => {
        leg.segments.map(segment => {
          airlines.some(airline => airline.id == segment.airline.id) ||
          airlines.push(segment.airline);
        });
        return airlines;
      }, [])
      .map(airlineLabel)
      .reduce((labels, label) =>
        [labels, " / ", label]
      )
    }
  </span>

const resultRowView = ({
  sequence,
  priority,
  price,
  currency,
  legs,
}) =>
  <span className="SearchResult-legs">
    <span className="SearchResult-legs-price">
      <Price
        price={price}
        currency={currency}
      />
      &nbsp;({sequence}, {priority})
    </span>
  {legs.map((leg, legIndex) =>
    <span className="SearchResult-leg" key={legIndex}>

      <span>
        {legIndex}
        &nbsp;
        ({leg.totalDuration})
        &nbsp;
      </span>

    {leg.segments.map((segment, segmentIndex) =>
      <span className="SearchResult-segment" key={segmentIndex}>
        <span className="SearchResult-airline">
          {segment.airline.id}
        </span>
        <span className="SearchResult-departTime">
          {segment.departTime.format("MMM d HH:mm")}
        </span>
        <span className="SearchResult-departAirport">
          {segment.departAirport}
        </span>
        <span className="SearchResult-flightSeparator">
          &raquo;
        </span>
        <span className="SearchResult-arriveTime">
          {segment.arriveTime.format("MMM d HH:mm")}
        </span>
        <span className="SearchResult-arriveAirport">
          {segment.arriveAirport}
        </span>
        {segmentIndex < leg.segments.length-1 &&
          <span className="SearchResult-separator">
            |
          </span>
        }
      </span>
    )}
    </span>
  )}
</span>;

const SearchResult = ({
  title,
  results,
  results: [{
    price,
    currency,
    legs,
  }],
}) => (
  <div>
  <article className="SearchResult card">

    <header>
      <span className="SearchResult-price">
        <Price
          price={price}
          currency={currency}
        />
      </span>
      <span className="SearchResult-airlines">
        {title || airlinesLabel([legs[0]])}
      </span>
    </header>

    <div className="SearchResult-rows">
      {results.slice(0, 10).map(resultRowView)}
    </div>

  </article>
  </div>
);

export default SearchResult;
