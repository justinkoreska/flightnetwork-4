import React from "react";

import Price from "components/price";

const resultRowView = ({
  price,
  currency,
  airline,
  departDate,
  returnDate,
}) =>
  <div className="SearchResult-segment">
    <span className="SearchResult-segment-price">
      <Price
        price={price}
        currency={currency}
      />
    </span>
    <span className="SearchResult-airline">
      {airline}
    </span>
    <span className="SearchResult-legs-date">
      {departDate.format("MMM d")}
    </span>
    <span className="SearchResult-flightSeparator">
      &raquo;
    </span>
    <span className="SearchResult-legs-date">
      {returnDate.format("MMM d")}
    </span>
  </div>;

const Flexdates = ({
  title,
  results,
  results: [{
    price,
    currency,
  }],
}) => (
  <div>
  <article className="SearchResult card">

    <header>
      <span className="SearchResult-price">
        <Price
          price={price}
          currency={currency}
        />
      </span>
      <span className="SearchResult-title">
        {title}
      </span>
    </header>

    <div className="SearchResult-rows">
      {results.slice(1, 11).map(resultRowView)}
    </div>

  </article>
  </div>
);

export default Flexdates;
