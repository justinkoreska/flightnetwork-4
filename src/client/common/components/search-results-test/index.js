import React from "react";
import { connect } from "react-redux";
import { actions, selectors } from "store/search";

import "./scss/index.scss";
import Filters from "components/search-filters";
import SearchResult from "./components/search-result";
import Flexdates from "./components/flexdates";

class SearchResults extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const hasFastest = this.props.fastest
      && (!this.props.best
          || this.props.best.sequence != this.props.fastest.sequence)
      && (!this.props.cheapest
          || this.props.cheapest.sequence != this.props.fastest.sequence);
    const hasCheapest = this.props.cheapest
      && (!this.props.best
          || this.props.cheapest.price < this.props.best.price);

    return (
      <div className="SearchResults">
        <Filters />
        <div className="SearchResults-results flex one two-500 three-800">
          {this.props.best &&
            <SearchResult key="best" results={[this.props.best]} title="Best" />
          }
          {hasFastest &&
            <SearchResult key="fastest" results={[this.props.fastest]} title="Fastest" />
          }
          {hasCheapest &&
            <SearchResult key="cheapest" results={[this.props.cheapest]} title="Cheapest" />
          }
          {!!this.props.flexdates.length &&
            <Flexdates key="flexible" results={this.props.flexdates} title="Flexible Dates" />
          }
          {!!this.props.alternates.length &&
            <SearchResult key="alternates" results={this.props.alternates} title="Nearby Airports" />
          }
          {this.props.results.map(result =>
            <SearchResult key={result.airline} results={result.itineraries} />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isSearching: selectors.isSearching(state),
  results: selectors.resultsByAirlineView(state),
  best: selectors.bestResult(state),
  fastest: selectors.fastestResult(state),
  cheapest: selectors.cheapestResult(state),
  alternates: selectors.alternateResults(state),
  flexdates: selectors.flexdatesView(state),
});

const mapDispatchToProps = dispatch => ({
  displayMore: e => {
    e.preventDefault();
    dispatch(actions.displayMore(10));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);
