import React from "react";
import { connect } from "react-redux";
import { actions as userActions } from "store/user";

class UserSigninView extends React.Component {

  constructor(props) {
    super(props);
  }

  onSignin(event) {
    const form = event.target.elements;
    this.props.signin(form.username.value, form.password.value)(event);
  }

  onSignout(event) {
    this.props.signout(event);
  }

  render() {
    const { user } = this.props;

    return (
      <div className="UserSignin">
        { user.isAuthenticated ?
          <div>
            welcome {user.data.username}
            <a href="#" onClick={this.onSignout.bind(this)}>signout</a>
          </div>
          :
          user.isLoading ?
            <span>loading</span>
            :
            <form onSubmit={this.onSignin.bind(this)}>
              <label>
                Username
                <input type="text" name="username" placeholder="username" />
              </label>
              <label>
                Password
                <input type="password" name="password" placeholder="password" />
              </label>
              { user.error && <span className="error">{user.error}</span> }
              <button type="submit">signin</button>
            </form>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  signin: (username, password) => e => {
    e.preventDefault();
    dispatch(userActions.signin(username, password));
  },
  signout: e => {
    e.preventDefault();
    dispatch(userActions.signout());
  },
});

const UserSignin = connect(mapStateToProps, mapDispatchToProps)(UserSigninView);

export default UserSignin;
