import React from "react";
import inject from "modules/inject";

import "./scss/index.scss";

@inject
class AirportInput extends React.Component {

  constructor(props) {
    super(props);
    for (const func of ["onBlur", "onFocus", "onChange", "onSelect", "onDebounce"])
      this[func] = this[func].bind(this);
    this.state = {
      input: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) {
      this.setState({
        selected: nextProps.value,
      });
    }
  }

  onBlur(event) {
    this.setState({
      focused: false,
    });
    setTimeout(() =>
      this.setState({
        airports: null
      }), 300
    );
  }

  onFocus(event) {
    this.setState({
      focused: true,
      airports: null,
      input: "",
    });
  }

  onChange(event) {
    if (!this.isEditing())
      return;
    this.setState({
      input: event.target.value,
    });
    if (event.target.value.length < 3)
      return;
    if (this.debounce)
      clearTimeout(this.debounce);
    this.debounce =
      setTimeout(this.onDebounce, 300);
  }

  onSelect(airport) {
    this.setState({
      selected: airport,
      airports: null,
    });
    this.props.onSelect(airport);
  }

  onDebounce() {
    const { input } = this.state;
    const { airportService } = this.context.services;

    airportService.find(input)
      .then(airports => {
        this.setState({
          airports
        });
      })
      .catch(error => console.log);
  }

  isEditing() {
    return this.state.focused || !this.state.selected;
  }

  airportLabel(airport = {}) {
    let label = "";

    if (airport.city && airport.code)
      label = `${airport.city} (${airport.code.toUpperCase()})`;
    else if (airport.code)
      label = airport.code.toUpperCase();

    return label;
  }

  render() {
    const {
      input,
      selected,
      airports,
    } = this.state;
    return (
      <span className="AirportInput">
        <input
          type="text"
          value={this.isEditing() ? input : this.airportLabel(selected)}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          onChange={this.onChange}
          placeholder={this.props.placeholder}
        />
        { airports &&
          <ul className="AirportInput-options">
            { airports.map(airport =>
              <li key={airport.code} onClick={() => this.onSelect(airport)}>
                {this.airportLabel(airport)}
              </li>
            ) }
          </ul>
        }
      </span>
    );
  }
}

export default AirportInput;
