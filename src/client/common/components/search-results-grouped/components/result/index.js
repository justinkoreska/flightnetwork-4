import React from "react";

import Price from "components/price";

import ResultDetails from "../result-details";

const airlineLabel = airline =>
  <span key={airline.id}>
    <i className="SearchResult-airlineLogo"><img src={airline.image}
    /></i>{airline.name}
  </span>

const airlinesLabel = legs =>
  <span>
    {legs
      .reduce((airlines, leg) => {
        leg.segments.map(segment => {
          airlines.some(airline => airline.id == segment.airline.id) ||
          airlines.push(segment.airline);
        });
        return airlines;
      }, [])
      .map(airlineLabel)
    }
  </span>

const renderStopovers = segments =>
  segments.length > 1
    ? segments.reduce((stops, segment, index) =>
        index ? [ ...stops, segment.departAirport ] : stops
      , [])
      .join(",")
    : "";

const ResultGroupView = ({
  currency,
  price,
  legs,
  group,
  isDetailsOpen,
  setDetailsOpen,
}) => (
  <article className="SearchResult">
    <div className="card">
      <header>

        <div className="SearchResult-price">
          <Price
            price={price}
            currency={currency}
          />
        </div>

        <div className="SearchResult-airlines">
          {airlinesLabel(legs)}
          <button
            className="SearchResult-details"
            onClick={e => setDetailsOpen(!isDetailsOpen)}
          >details</button>
        </div>

      </header>
      <footer>

        <div className="SearchResult-legs">

          {legs.map((leg, legIndex) =>
            <div className="SearchResult-leg" key={legIndex}>

              <div className="SearchResult-depart">
                <span className="SearchResult-departSeparator"></span>
                <span className="SearchResult-departAirport">{
                  leg.segments[0].departAirport
                }</span>
              </div>

              <div className="SearchResult-connection">
              </div>

              <div className="SearchResult-stopover">{
                renderStopovers(leg.segments)
              }</div>

              <div className="SearchResult-connection">
              </div>

              <div className="SearchResult-arrive">
                <span className="SearchResult-arriveSeparator"></span>
                <span className="SearchResult-arriveAirport">{
                  leg.segments[leg.segments.length-1].arriveAirport
                }</span>
              </div>

            </div>
          )}

        </div>
        
      </footer>
    </div>

    {isDetailsOpen &&
      <div className="modal">
        <input id="detailsOpen" type="checkbox" checked="checked" />
        <label for="detailsOpen" className="overlay"></label>
        <article>
          <header>
            <h3>Choose times</h3>
            <label
              className="close"
              onClick={e => setDetailsOpen(false)}
            >&times;</label>
          </header>
          <section className="content">
            <ResultDetails {...{
              legs,
              group,
            }} />
          </section>
        </article>
      </div>
    }

  </article>
);

class ResultGroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isDetailsOpen: false,
    };
  }

  setDetailsOpen(isDetailsOpen) {
    this.setState({
      isDetailsOpen,
    });
  }

  render() {
    return ResultGroupView({
      ...this.props,
      ...this.state,
      setDetailsOpen: this.setDetailsOpen.bind(this),
    });
  }

}

export default ResultGroup;
