import React from "react";

class ResultDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedGroupIndex: 0,
    };
  }

  onTimeSelect(event) {
    this.setState({ selectedGroupIndex: event.target.value });
  }

  getDistinctTimes(legIndex, segmentIndex, key) {
    return this.props.group
      .map((result, index) => ({
        index,
        time: result.legs[legIndex].segments[segmentIndex][key],
      }))
      .sort((a, b) =>
        a.time.isBefore(b.time) ? -1 : 1
      )
      .map(distinctTime => ({
        index: distinctTime.index,
        time: distinctTime.time.format("MMM D HH:mm"),
      }))
      .reduce((distinctTimes, distinctTime) => {
        distinctTimes.some(x => x.time == distinctTime.time) ||
        distinctTimes.push(distinctTime);
        return distinctTimes;
      }, []);
  }

  render() {
    return ResultDetailsView({
      ...this.props,
      ...this.state,
      onTimeSelect: this.onTimeSelect.bind(this),
      getDistinctTimes: this.getDistinctTimes.bind(this),
    });
  }
}

const MultiTimeView = ({
  distinctTimes,
  onTimeSelect,
  selectedGroupIndex,
}) => (
  distinctTimes.length == 1
    ? <span>{distinctTimes[0].time}</span>
    : <select value={selectedGroupIndex} onChange={onTimeSelect}>
        {distinctTimes.map(distinctTime =>
          <option key={distinctTime.index} value={distinctTime.index}>
            {distinctTime.time}
          </option>
        )}
      </select>
);

const ResultDetailsView = ({
  legs,
  onTimeSelect,
  selectedGroupIndex,
  getDistinctTimes,
}) => (
  <div className="ResultDetails">

      {legs.map((leg, legIndex) =>
        <div className="ResultDetails-leg" key={legIndex}>

          <span className="ResultDetails-legTitle">
            Flight {legIndex+1}
          </span>

        {leg.segments.map((segment, segmentIndex) =>
          <div className="ResultDetails-segment" key={segmentIndex}>

            <span className="ResultDetails-departSeparator"></span>

            <span className="ResultDetails-departAirport">
              {segment.departAirport}
            </span>

            <span className="ResultDetails-departTime">
              <MultiTimeView
                distinctTimes={getDistinctTimes(legIndex, segmentIndex, "departTime")}
                selectedGroupIndex={selectedGroupIndex}
                onTimeSelect={onTimeSelect}
              />
            </span>
            
            <span>
              &rarr;
            </span>

            <span className="ResultDetails-arriveSeparator"></span>

            <span className="ResultDetails-arriveAirport">
              {segment.arriveAirport}
            </span>

            <span className="ResultDetails-arriveTime">
              <MultiTimeView
                distinctTimes={getDistinctTimes(legIndex, segmentIndex, "arriveTime")}
                selectedGroupIndex={selectedGroupIndex}
                onTimeSelect={onTimeSelect}
              />
            </span>

          </div>
        )}

        </div>
      )}

  </div>
);

export default ResultDetails;
