import React from "react";
import { connect } from "react-redux";
import {
  TransitionGroup,
  CSSTransition,
} from "react-transition-group";

import { actions, selectors } from "store/search";

import "./scss/index.scss";
import Filters from "components/search-filters";
import Result from "./components/result";

class SearchResults extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      results: props.results,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this._showTimeout)
      clearTimeout(this._showTimeout);

    const next = this.diffResults(nextProps.results);
    this.showNextResult(nextProps.results, next);
  }

  diffResults(results) {
    const keepResults = [];
    for (const index in results) {
      const result = results[index];
      if (JSON.stringify(result) == JSON.stringify(this.state.results[index]))
        keepResults.push(this.state.results[index]);
      else
        break;
    }
    this.setState({ results: keepResults });
    return keepResults.length;
  }

  showNextResult(results, index) {
    const result = results[index];
    if (!result) return;
    this._showTimeout = setTimeout(() => {
      this.setState({ results: [...this.state.results, result] });
      this.showNextResult(results, ++index);
    }, 200);
  }

  render() {
    const hasMore = this.props.displayCount == this.props.results.length;
    return (
      <div className="SearchResults">
        {!this.props.isSearching &&
          <Filters />
        }
        <TransitionGroup className="flex one two-700 three-1000">
          {this.state.results.map((result, index) =>
            <CSSTransition
              key={index}
              classNames="SearchResult"
              timeout={{ enter: 500, exit: 300 }}
              >
              <Result {...result} />
            </CSSTransition>
          )}
        </TransitionGroup>
        {!this.props.isSearching && hasMore &&
          <button onClick={this.props.displayMore}>
            Show more
          </button>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  results: selectors.resultsGroupedView(state),
  displayCount: selectors.displayCount(state),
  isSearching: state.search.isSearching,
});

const mapDispatchToProps = dispatch => ({
  displayMore: e => {
    e.preventDefault();
    dispatch(actions.displayMore(10));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);
