import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import inject from "modules/inject";
import { actions as localeActions } from "store/locale";
import { actions as currencyActions } from "store/currency";

@inject
class LocaleHandler extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.update(this.props);
  }

  componentWillReceiveProps(props) {
    this.update(props);
  }

  update(props) {
    const {
      cookieService,
    } = this.context.services;
    const {
      setLocale,
      selectLocale,
      setCurrency,
      defaultLocale,
      defaultCurrency,
      supportedLocales,
      location: { pathname: path },
    } = props;

    const localeMatch =
      /^\/(\w{2}\-\w{2})/.exec(path);

    const cookieLocale =
      cookieService.getCookie("locale");

    const userLocale =
      cookieLocale ||
      navigator && (
        navigator.languages && navigator.languages[0] ||
        navigator.language
      ) ||
      defaultLocale;

    const locale = localeMatch
      ? localeMatch[1]
      : userLocale;

    if (!localeMatch)
      selectLocale(locale);
    else if (locale != props.selectedLocale)
      setLocale(locale);

    const cookieCurrency =
      cookieService.getCookie("currency");

    const currency =
      cookieCurrency ||
      (supportedLocales[locale] || {}).currency ||
      defaultCurrency;

    if (currency != props.selectedCurrency)
      setCurrency(currency);
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  supportedLocales: state.config.locale.supported,
  defaultLocale: state.config.locale.default,
  selectedLocale: state.locale.selected,
  defaultCurrency: state.config.currency.default,
  selectedCurrency: state.currency.selected,
});

const mapDispatchToProps = dispatch => ({
  setLocale: locale => dispatch(localeActions.setLocale(locale)),
  selectLocale: locale => dispatch(localeActions.selectLocale(locale)),
  setCurrency: currency => dispatch(currencyActions.setCurrency(currency)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LocaleHandler);
