import React from "react";
import { connect } from "react-redux";

import { actions, selectors } from "store/search";

import "./index.scss";

const isSelected = (selected, value) =>
  selected && selected.some(x => x == JSON.stringify(value));

const departureTimesPeriodLabel = period => ([
  "Night (12am - 5am)",
  "Morning (5am - 12pm)",
  "Afternoon (12pm - 6pm)",
  "Evening (6pm - 12am)",
][period] || "");

class SearchFilters extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
    };
  }

  toggleCollapse(e) {
    e && e.preventDefault();
    this.setState({
      isCollapsed: !this.state.isCollapsed,
    });
  }

  selectOnly(filterName, filterValue) {
    this.props.filters[filterName]
      .forEach(value =>
        this.props.setFilter(filterName, value, value != filterValue)
      );
    this.props.setFilterMode(filterName, true);
  }

  render() {
    return SearchFiltersView({
      ...this.props,
      ...this.state,
      toggleCollapse: this.toggleCollapse.bind(this),
      selectOnly: this.selectOnly.bind(this),
    });
  }
}

const SearchFiltersView = ({
  filters = {},
  selected = {},
  exclusive = {},
  setFilter,
  setFilterMode,
  toggleCollapse,
  isCollapsed,
  selectOnly,
}) =>
  <div className="SearchFilters">
    <h3>
      Filters
      <span>
        <button className="pseudo" onClick={toggleCollapse}>
          {isCollapsed ? "▶" : "▼"}
        </button>
      </span>
    </h3>
    {!isCollapsed &&
    <div>
      {filters.stops &&
      <div>
        <h4>Stops</h4>
        {filters.stops.map(stops =>
          <div key={stops}>
            <label>
              <input
                type="checkbox"
                onChange={e => setFilter("stops", stops, !e.target.checked)}
                checked={!isSelected(selected.stops, stops)}
              />
              <span className="checkable">
                {stops}
              </span>
            </label>
          </div>
        )}
      </div>}
      {filters.departure &&
      <div>
        <h4>Times</h4>
        {filters.departure
          .reduce((legs, departure) => {
            legs[departure.leg] = legs[departure.leg] || [];
            legs[departure.leg].push(departure);
            return legs;
          }, [])
          .map((leg, legIndex, legs) =>
            <div>
              <h5>Flight {legIndex+1}</h5>
              {leg.sort((a, b) => a.period > b.period ? 1 : -1).map(departure =>
                <div key={`${departure.leg}-${departure.period}`}>
                  <label>
                    <input
                      type="checkbox"
                      onChange={e => setFilter("departure", departure, !e.target.checked)}
                      checked={!isSelected(selected.departure, departure)}
                    />
                    <span className="checkable">
                      {departureTimesPeriodLabel(departure.period)}
                    </span>
                  </label>
                </div>
              )}
            </div>
          )
        }
      </div>}
      {filters.airline &&
      <div>
        <h4>Airlines</h4>
        {filters.airline.map(airline =>
          <div key={airline.id}>
            <label>
              <input
                type="checkbox"
                onChange={e => setFilter("airline", airline, !e.target.checked)}
                checked={!isSelected(selected.airline, airline)}
              />
              <span className="checkable">
                {airline.name}
              </span>
            </label>
            <span className="SearchFilters-rollover">
              <button
                className="SearchFilters-only"
                onClick={e => selectOnly("airline", airline)}
              >
                only
              </button>
            </span>
          </div>
        )}
        <div key="mode">
          <label>
            <input
              type="checkbox"
              onChange={e => setFilterMode("airline", !e.target.checked)}
              checked={!exclusive["airline"]}
            />
            <span className="checkable">
              Multiple airlines
            </span>
          </label>
        </div>
      </div>}
    </div>}
  </div>;

const mapState = state => ({
  filters: selectors.filtersView(state),
  selected: selectors.selectedFilters(state),
  exclusive: selectors.exclusiveFilters(state),
});

const mapDispatch = dispatch => ({
  setFilter: (filter, value, isSelected) => dispatch(actions.setFilter(filter, value, isSelected)),
  setFilterMode: (filter, isExclusive) => dispatch(actions.setFilterMode(filter, isExclusive)),
});

export default connect(mapState, mapDispatch)(SearchFilters);
