import React from "react";

import "./scss/index.scss";

import LocaleSelect from "./components/locale-select";
import CurrencySelect from "./components/currency-select";

const HeaderView = () => (
  <header className="Header">
    <nav>
      <a href="/" className="brand">
        <div className="Header-logo" />
      </a>

      <input id="bmenub" type="checkbox" className="show" />
      <label htmlFor="bmenub" className="burger pseudo button">&#9776;</label>

      <div className="menu">
        <LocaleSelect />
        <CurrencySelect />
      </div>
    </nav>
  </header>
);

export default HeaderView;
