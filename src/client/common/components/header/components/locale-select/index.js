import React from "react";
import { connect } from "react-redux";

import { actions } from "store/locale";

const LocaleSelect = ({
  locales,
  selected,
  select,
}) => (
  <span className="LocaleSelect">
  {Object.keys(locales).map(locale =>
    <a href="#"
      className="LocaleSelect-locale pseudo button"
      key={locale}
      onClick={select(locale)}
      >
      {locales[locale].name}
      {locale == selected && "*"}
    </a>
  )}
  </span>
);

const mapStateToProps = state => ({
  locales: state.config.locale.supported,
  selected: state.locale.selected,
});

const mapDispatchToProps = dispatch => ({
  select: locale => e => {
    e.preventDefault();
    dispatch(actions.selectLocale(locale));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LocaleSelect);
