import React from "react";
import { connect } from "react-redux";

import { actions } from "store/currency";

const CurrencySelect = ({
  currencies,
  selected,
  select,
}) => (
  <span className="CurrencySelect">
  {Object.keys(currencies).map(currency =>
    <a href="#"
      className="CurrencySelect-currency pseudo button"
      key={currency}
      onClick={select(currency)}
      >
      {currency}
      {currency == selected && "*"}
    </a>
  )}
  </span>
);

const mapStateToProps = state => ({
  currencies: state.config.currency.supported,
  selected: state.currency.selected,
});

const mapDispatchToProps = dispatch => ({
  select: currency => e => {
    e.preventDefault();
    dispatch(actions.setCurrency(currency));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrencySelect);
