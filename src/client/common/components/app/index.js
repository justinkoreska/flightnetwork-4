import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";

import ReduxIntlProvider from "./redux-intl-provider";

class App extends React.Component {

  // ChildContext as a dependency container
  static childContextTypes = {
    services: PropTypes.object
  };

  getChildContext() {
    return {
      services: this.props.services
    };
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <ReduxIntlProvider>
          {this.props.router}
        </ReduxIntlProvider>
      </Provider>
    );
  }
}

export default App;
