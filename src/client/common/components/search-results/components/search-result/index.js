import React from "react";

import Price from "components/price";

const airlineLabel = airline =>
  <span key={airline.id}>
    <i className="SearchResult-airlineLogo">
      <img src={airline.image} />
    </i>
    {airline.name}
  </span>

const airlinesLabel = legs =>
  <span>
    {legs
      .reduce((airlines, leg) => {
        leg.segments.map(segment => {
          airlines.some(airline => airline.id == segment.airline.id) ||
          airlines.push(segment.airline);
        });
        return airlines;
      }, [])
      .map(airlineLabel)
      .reduce((labels, label) =>
        [labels, " / ", label]
      )
    }
  </span>

const SearchResult = ({
  currency,
  price,
  legs,
}) => (
  <div className="SearchResult">

    <span className="SearchResult-price">
      <Price
        price={price}
        currency={currency}
      />
    </span>

    <span className="SearchResult-airlines">
      {airlinesLabel(legs)}
    </span>

    <span className="SearchResult-legs">

    {legs.map((leg, legIndex) =>
      <span className="SearchResult-leg" key={legIndex}>

      {leg.segments.map((segment, segmentIndex) =>
        <span className="SearchResult-segment" key={segmentIndex}>

          <span className="SearchResult-departTime">
            {segment.departTime.format("HH:mm")}
          </span>
          <span className="SearchResult-departAirport">
            {segment.departAirport}
          </span>
          <span className="SearchResult-flightSeparator">
            &raquo;
          </span>
          <span className="SearchResult-arriveTime">
            {segment.arriveTime.format("HH:mm")}
          </span>
          <span className="SearchResult-arriveAirport">
            {segment.arriveAirport}
          </span>

        </span>
      )}

      </span>
    )}

    </span>

  </div>
);

export default SearchResult;
