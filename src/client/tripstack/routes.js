import React from "react";
import {
  Route,
  Switch,
} from "react-router-dom";

import HomePage from "./components/home-page";
import SearchPage from "./components/search-page";
import ErrorPage from "components/error-page";
import LocaleHandler from "components/locale-handler";

const path = {
  locale: "/:locale(\\w{2,3}\\-\\w{2,3})",
  from: "/:from(\\w{3})",
  to: "/:to(\\w{3})",
  depart: "/:depart(\\d{8})",
  return: "/:return(\\d{0,8})",
};

export default (
  <div>
    <Route component={LocaleHandler} />
    <Switch>
      <Route exact path={path.locale} component={HomePage} />
      <Route path={path.locale + "/search" + path.from + path.to + path.depart + path.return} component={SearchPage} />
      <Route path={path.locale + "/error"} component={ErrorPage} />
      <Route component={ErrorPage} />
    </Switch>
  </div>
);
