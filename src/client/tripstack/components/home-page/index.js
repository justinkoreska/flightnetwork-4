import React from "react";
import { FormattedMessage } from "react-intl";

import SearchForm from "components/search-form";

import Header from "../header";

import "./scss/index.scss";
import locale from "./messages";

const HomePage = () =>
  <div className="HomePage">
    <Header/>
    <section className="HomePage-content">
      <h1>
        <FormattedMessage {...locale.Heading1} />
      </h1>
      <SearchForm />
    </section>
  </div>;

export default HomePage;
