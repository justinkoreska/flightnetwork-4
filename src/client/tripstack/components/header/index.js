import React from "react";

import CurrencySelect from "components/header/components/currency-select";

import "./scss/index.scss";

const HeaderView = () => (
  <header className="Header">
    <nav>
      <a href="/" className="brand">
        <div className="Header-logo" />
      </a>
      <div className="menu">
        <CurrencySelect />
      </div>
    </nav>
  </header>
);

export default HeaderView;
