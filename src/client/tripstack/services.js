import Bottle from "bottlejs";

import "whatwg-fetch";

import config from "./config";
import UserService from "services/user";
import AirportService from "services/airport";
import SearchService from "services/search/ota";
import LocaleService from "services/locale";
import CurrencyService from "services/currency";
import CookieService from "services/cookie";

const kernel = new Bottle();

kernel.service("config", () => config);
kernel.service("location", () => window.location);
kernel.service("history", () => window.history);
kernel.service("fetch", () => fetch.bind(window));
kernel.service("document", () => document);

kernel.service("userService", UserService, "config", "fetch");
kernel.service("airportService", AirportService, "config", "fetch");
kernel.service("searchService", SearchService, "config", "fetch");
kernel.service("localeService", LocaleService, "config", "fetch");
kernel.service("currencyService", CurrencyService, "config", "fetch");
kernel.service("cookieService", CookieService, "config", "document");

export default kernel.container;
