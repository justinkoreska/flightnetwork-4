
export default {
  apiBase: "/api",
  fnxmlBase: "https://fnxml.flightnetwork.com/api",
  search: {
    pollSeconds: 3,
    timeoutSeconds: 60,
  },
  locale: {
    default: "en-CA",
    supported: {
      "en-CA": { name: "English (Canada)", currency: "CAD" },
      "fr-CA": { name: "French (Canada)", currency: "CAD" },
      "en-US": { name: "English (USA)", currency: "USD" },
    },
  },
  currency: {
    default: "CAD",
    supported: {
      "CAD": { symbol: "$", name: "Canadian Dollar" },
      "USD": { symbol: "$", name: "U.S. Dollar" },
      "GBP": { symbol: "£", name: "British Pound" },
    },
  },
  cookie: {
    domain: "",
  },
};
