import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import moment from "moment";

import { actions as search } from "store/search";

import Header from "components/header";
import SearchForm from "components/search-form";
import SearchResults from "components/search-results-test";

import "./scss/index.scss";
import locale from "./messages";

class SearchPage extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.match.params) {
      const { from, to } = this.props.match.params;
      if (from && to)
        this.props.setForm(this.props.match.params);
    }
  }

  render() {
    return (
      <div className="SearchPage">
        <Header/>
        <section className="SearchPage-content">
          <h1>
            <FormattedMessage {...locale.Heading1} />
          </h1>
          <SearchForm />
          <SearchResults />
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  setForm: form => {
    dispatch(search.updateLeg(0, { from: { code: form.from }, to: { code: form.to }, date: moment(form.depart) }));
    dispatch(search.updateLeg(1, { from: { code: form.to }, to: { code: form.from }, date: moment(form.return) }));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
