import React from "react";

import "./scss/index.scss";
import Header from "components/header";

const BookingPage = () => (
  <section className="BookingPage">
    <Header/>
    <div className="BookingPage-content">
      <h1>Book Your Flight</h1>
    </div>
  </section>
);

export default BookingPage;
