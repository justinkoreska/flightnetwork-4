import React from "react";
import { FormattedMessage } from "react-intl";

import Header from "components/header";
import SearchForm from "components/search-form";

import "./scss/index.scss";
import locale from "./messages";
import SamplePrice from "./components/sample-price";

const HomePage = () =>
  <div className="HomePage">
    <Header/>
    <section className="HomePage-content">
      <h1>
        <FormattedMessage {...locale.Heading1} />
      </h1>
      <SearchForm />
      <SamplePrice />
    </section>
  </div>;

export default HomePage;
