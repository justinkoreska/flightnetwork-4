import React from "react";

import Price from "components/price";

class SamplePrice extends React.Component {

  static value = 999.99;

  constructor(props) {
    super(props);
    this.state = { currency: "USD" };
  }

  changeCurrency(currency) {
    this.setState({ currency });
  }

  render() {
    return (
      <div className="SamplePrice">
        Sample price
        <div className="SamplePrice-price">
          <Price
            price={SamplePrice.value}
            currency={this.state.currency}
          />
        </div>
        <div>
          <span>
            {SamplePrice.value}
          </span>
          {["USD", "CAD", "EUR"].map(currency =>
            <span key={currency}>
              &nbsp;
              <a href="#" onClick={e => { e.preventDefault(); this.changeCurrency(e.target.innerText); }}>
                {currency === this.state.currency
                  ? <strong>{currency}</strong>
                  : currency }
              </a>
            </span>
          )}
        </div>
      </div>
    );
  }
}

export default SamplePrice;
