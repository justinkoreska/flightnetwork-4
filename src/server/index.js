import express from "express";
import bodyParser from "body-parser";

import sites from "../sites";
import routes from "./routes";

const siteLogger = (request, response, next) => {
  const host = request.header("Host");
  const site = sites.find(x => x.host === host);
  if (site)
    request.site = site.name;

  console.log(`${host} ${new Date()} ${request.ip} ${request.method} ${request.path}`);
  
  next();
};

const server = express();

server.use(bodyParser.json());
server.use(siteLogger);

Object.keys(routes).forEach(route => {
  server.use(route, routes[route]);
});

console.log("Starting server on 8080");
server.listen("8080");
