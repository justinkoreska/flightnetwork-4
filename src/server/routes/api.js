import express from "express";
import uuid from "uuid";

const router = express.Router();

const users = {};

router.post("/auth", (request, response) => {
  const { username, password } = request.body || {};

  console.log("/auth", { username, password });

  if (username && username == password) {
    const token = uuid.v4();
    users.token = { username };
    setTimeout(
      () => response.send({ token }),
      2000
    );
  } else {
    response
      .status(401)
      .send({ error: "Not authorized" });
  }
});

export { router };
