import fs from "fs";
import path from "path";
import express from "express";
import handlebars from "handlebars";

const router = express.Router();

router.get("*", (request, response) => {
  const template = handlebars.compile(
    fs.readFileSync(
      path.join(__dirname, request.site, "templates", "index.html"),
      "utf8"
    )
  );
  response.send(
    template({ title: "test" })
  );
});

export { router };
