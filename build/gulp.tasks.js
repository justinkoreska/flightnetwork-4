import gulp from "gulp";
import mocha from "gulp-spawn-mocha";
import rimraf from "rimraf";
import webpack from "webpack";
import merge from "webpack-merge";

import translations from "./gulp-translations";

import commonConfig from "./config.common";
import developmentConfig from "./config.site.development";
import productionConfig from "./config.site.production";

import sites from "../src/sites";

let config = commonConfig; // updated in site task

sites.forEach(site =>
  gulp.task(site.name, () => {
    config = merge(
      commonConfig,
      "production" == process.env.NODE_ENV
        ? productionConfig(site.name, commonConfig)
        : developmentConfig(site.name, commonConfig)
    );
    return gulp.start("client");
  })
);

gulp.task("clean", done => {
  rimraf(config.path.output, done);
});

gulp.task("test", () =>
  gulp.src(config.path.test, { read: false })
    .pipe(mocha(config.mocha))
);

gulp.task("translations", () =>
  gulp.src(`src/client/${config.site.name}/**/components/**/messages.js`)
    .pipe(translations(`src/client/${config.site.name}/translations.csv`))
    .pipe(gulp.dest(config.path.output + "/assets/"))
);

gulp.task("client", ["test", "translations"], done => {
  config.site.webpack.watch = process.argv[3] === "--watch";
  webpack(config.site.webpack, (err, stats) => {
    console.log(stats.toString(config.webpackStatsPresets.minimal));
    if (!config.site.webpack.watch)
      done();
  })
});

gulp.task("server", done => {
  config = merge(commonConfig, )
  webpack(config.server.webpack, (err, stats) => {
    console.log(stats.toString(config.webpackStatsPresets.minimal));
    done();
  });
});

