import path from "path";
import { SourceMapDevToolPlugin } from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

export default (site, common) => ({
  mode: "development",
  path: {
    output: `dist/${site}/`,
    test: `src/${site}/**/*.test.js`,
  },
  site: {
    name: site,
    webpack: {
      mode: "development",
      entry: {
        app: `./src/client/${site}/index.js`,
      },
      output: {
        filename: "[name]-[chunkhash].js",
        path: path.resolve(common.path.output, site, "assets"),
        publicPath: "/assets/",
      },
      resolve: {
        modules: [
          "node_modules",
          "src/client/common",
          `src/client/${site}`,
        ],
      },
      optimization: {
        splitChunks: {
          cacheGroups: {
            vendor: {
              name: "vendor",
              test: /node_modules/,
              chunks: "all",
            },
            style: {
              name: "style",
              test: /\.s?css$/,
              chunks: "all",
            },
          },
        },
      },
      devtool: "inline-cheap-module-source-map",
      plugins: [
        new MiniCssExtractPlugin({
          filename: "[name]-[contenthash].css",
        }),
        // new SourceMapDevToolPlugin({
        //   exclude: /vendor/,
    		// 	 module: true,
    		// 	 columns: false,
    		// 	 noSources: false,
        //   lineToLine: false,
        // }),
        new HtmlWebpackPlugin({
          filename: "../templates/index.html",
          template: `src/client/${site}/index.html`,
          favicon: `src/client/${site}/scss/img/favicon.png`,
        }),
      ],
      module: {
        rules: [{
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader",
        }, {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)(\?\S*)?$/,
          loader: "file-loader",
          query: {
            name: "[hash].[ext]",
          },
        }, {
          test: /\.s?css$/,
          use: [{
            loader: MiniCssExtractPlugin.loader,
          }, {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          }, {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              includePaths: [
                "node_modules",
                "src/client/common",
                `src/client/${site}`,
              ],
              data: "@import 'scss/variables';",
            },
          }],
        }],
      },
    },
  },
});
