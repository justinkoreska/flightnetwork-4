import fs from "fs";
import File from "vinyl";
import through from "through2";
import CsvParse from "csv-parse/lib/sync";

const translations = source => {

  const messageIds = [];

  const buffer = (file, encoding, callback) => {
    const messages =
      require(file.path).default;
    messageIds.push(
      ...Object
        .values(messages)
        .map(message => message.id)
    );
    callback();
  };

  const end = function(callback) {
    let translations;
    try {
      translations = fetchTranslations();
    } catch(e) {
      callback(e);
    }

    Object
      .keys(translations)
      .forEach(locale => {
        console.log(`Mapping ${locale}`);
        this.push(
          new File({
            path: `${locale}.json`,
            contents: new Buffer(JSON.stringify(
              mapMessageIds(translations[locale])
            )),
          })
        );
      });
    callback();
  };

  const fetchTranslations = () => {
    const translations = {};
    const contents = fs.readFileSync(source, "UTF-8");
    const rows = CsvParse(contents, { columns: true });

    rows.forEach(row => {
      const columns = Object
        .keys(row)
        .filter(key => "id" != key);
      columns.forEach(column => {
        translations[column] = translations[column] || {};
        translations[column][row.id] = row[column];
      });
      if (!messageIds.includes(row.id))
        console.log(`Unused id ${row.id}`);
    });

    return translations;
  };

  const mapMessageIds = translations => {
    const mappedIds = {};

    messageIds.forEach(id => {
      if (translations[id]) {
        mappedIds[id] = translations[id];
      } else {
        mappedIds[id] = id;
        console.log(` missing ${id}`);
      }
    });

    return mappedIds;
  }

  return through.obj(buffer, end);
};

export default translations;
